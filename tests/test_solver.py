from mock import call, Mock
import unittest

from challenge.solver import Solver


class SolverTestCase(unittest.TestCase):
    def setUp(self):
        self.mock_client = Mock()

    def test_it_guesses_letters_in_the_letter_sequence(self):

        self.mock_client.make_guess.return_value = {
            "word": "***",
            "total_word_count": 1,
            "wrong_guess_count_of_current_word": 1
        }
        solver = Solver(api_client=self.mock_client, guess_sequence=["A", "B"])
        session_id = "g1"
        solver.solve_word(word="***", session_id=session_id, wrong_guess_limit=3)
        self.mock_client.make_guess.assert_has_calls([call(session_id, "A"), call(session_id, "B")])
        self.assertEquals(self.mock_client.make_guess.call_count, 2)

    def test_it_makes_as_many_guesses_as_the_game_allows_for_each_word(self):

        self.mock_client.make_guess.side_effect = [
            {
                "word": "***",
                "total_word_count": 1,
                "wrong_guess_count_of_current_word": 1
            },
            {
                "word": "***",
                "total_word_count": 1,
                "wrong_guess_count_of_current_word": 2
            },
        ]

        solver = Solver(api_client=self.mock_client, guess_sequence=["A", "B", "C"])
        session_id = "g2"
        solver.solve_word(word="***", session_id=session_id, wrong_guess_limit=2)
        self.mock_client.make_guess.assert_has_calls([call(session_id, "A"), call(session_id, "B")])
        self.assertEquals(self.mock_client.make_guess.call_count, 2)

    def test_it_makes_as_many_guesses_as_the_guess_sequence(self):

        self.mock_client.make_guess.side_effect = [
            {
                "word": "***",
                "total_word_count": 1,
                "wrong_guess_count_of_current_word": 1
            },
            {
                "word": "***",
                "total_word_count": 1,
                "wrong_guess_count_of_current_word": 2
            },
        ]

        solver = Solver(api_client=self.mock_client, guess_sequence=["A"])
        session_id = "g2"
        solver.solve_word(word="***", session_id=session_id, wrong_guess_limit=2)
        self.mock_client.make_guess.assert_has_calls([call(session_id, "A")])
        self.assertEquals(self.mock_client.make_guess.call_count, 1)

    def test_it_stops_guessing_if_all_letters_are_revealed(self):

        self.mock_client.make_guess.side_effect = [
            {
                "word": "A*",
                "total_word_count": 1,
                "wrong_guess_count_of_current_word": 0
            },
            {
                "word": "AB",
                "total_word_count": 1,
                "wrong_guess_count_of_current_word": 0
            },
        ]
        solver = Solver(api_client=self.mock_client, guess_sequence=["A", "B", "C"])
        session_id = "g2"
        solver.solve_word(word="**", session_id=session_id, wrong_guess_limit=60)
        self.mock_client.make_guess.assert_has_calls([call(session_id, "A"), call(session_id, "B")])
        self.assertEquals(self.mock_client.make_guess.call_count, 2)
