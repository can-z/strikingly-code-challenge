from nose.plugins.attrib import attr
import unittest

from challenge.api_client import APIClient


@attr("production_api")
class APIClientTestCases(unittest.TestCase):

    def setUp(self):
        self.client = APIClient("https://strikingly-hangman.herokuapp.com/game/on")

    def test_start_game(self):
        game_info = self.client.start_game()
        self.assertIsInstance(game_info["session_id"], unicode)
        self.assertIsInstance(game_info["number_of_words_to_guess"], int)
        self.assertIsInstance(game_info["number_of_guess_allowed_for_each_word"], int)

    def test_get_word(self):
        session_id = self.client.start_game()["session_id"]
        word_info = self.client.get_word(session_id)

        self.assertIsInstance(word_info["word"], unicode)
        self.assertIsInstance(word_info["total_word_count"], int)
        self.assertIsInstance(word_info["wrong_guess_count_of_current_word"], int)

    def test_make_guess(self):
        session_id = self.client.start_game()["session_id"]
        self.client.get_word(session_id)
        guess_result = self.client.make_guess(session_id, "A")

        self.assertIsInstance(guess_result["word"], unicode)
        self.assertIsInstance(guess_result["total_word_count"], int)
        self.assertIsInstance(guess_result["wrong_guess_count_of_current_word"], int)

    def test_get_result(self):
        session_id = self.client.start_game()["session_id"]
        game_result = self.client.get_result(session_id)

        self.assertIsInstance(game_result["total_word_count"], int)
        self.assertIsInstance(game_result["correct_word_count"], int)
        self.assertIsInstance(game_result["total_wrong_guess_count"], int)
        self.assertIsInstance(game_result["score"], int)

    def test_submit_result(self):
        session_id = self.client.start_game()["session_id"]
        game_result = self.client.submit_result(session_id)

        self.assertIsInstance(game_result["total_word_count"], int)
        self.assertIsInstance(game_result["correct_word_count"], int)
        self.assertIsInstance(game_result["total_wrong_guess_count"], int)
        self.assertIsInstance(game_result["score"], int)
        self.assertIsInstance(game_result["datetime"], unicode)
