from mock import MagicMock, Mock
import unittest

from challenge.playthrough import Playthrough


class PlaythroughStartTestCase(unittest.TestCase):

    def setUp(self):
        self.mock_client = MagicMock()
        self.mock_client.start_game.return_value = {
            "session_id": "1",
            "number_of_words_to_guess": 80,
            "number_of_guess_allowed_for_each_word": 10,
        }

    def test_it_gets_and_solves_each_word(self):
        self.mock_client.get_word.return_value = {
            "word": "****",
            "total_word_count": 1,
            "wrong_guess_count_of_current_word": 0,
        }

        mock_solver = Mock()
        playthrough = Playthrough(api_client=self.mock_client, solver=mock_solver)
        playthrough.start()
        self.mock_client.start_game.assert_called_with()
        self.mock_client.get_word.assert_called_with("1")
        self.assertEquals(self.mock_client.get_word.call_count, 80)
        mock_solver.solve_word.assert_called_with(
            session_id="1",
            word="****",
            wrong_guess_limit=10,
        )
        self.assertEquals(mock_solver.solve_word.call_count, 80)
        self.mock_client.get_result.assert_called_with("1")
