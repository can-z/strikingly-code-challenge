# Setup Instructions

1. Install Python 2.7
2. Install pip
3. Go to the project root
4. Run `pip install -r requirements.txt`
5. Set environment variable `STRIKINGLY_USERNAME` to a suitable username
6. Run `python main.py`

# Notes

- To run all tests without hitting the real API, run `nosetests -A 'not production_api'`
