from challenge.api_client import APIClient
from challenge.playthrough import Playthrough
from challenge.solver import Solver


if __name__ == "__main__":
    real_api = APIClient("https://strikingly-hangman.herokuapp.com/game/on")
    playthrough = Playthrough(api_client=real_api, solver=Solver(api_client=real_api))
    print playthrough.start()
