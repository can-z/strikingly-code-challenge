import requests
import os
import json

from challenge.exceptions import APIError


class APIClient(object):

    def __init__(self, url):
        self.game_end_point_url = url

    def start_game(self):
        response = requests.post(
            self.game_end_point_url,
            data=json.dumps({
                "playerId": os.environ["STRIKINGLY_USERNAME"],
                "action": "startGame",
            }),
            headers={"Content-Type": "application/json"}
        )
        if response.status_code != 200:
            raise APIError("status_code: {}".format(response.status_code))
        response_data = json.loads(response.text)
        game_info = {
            "session_id": response_data["sessionId"],
            "number_of_words_to_guess": response_data["data"]["numberOfWordsToGuess"],
            "number_of_guess_allowed_for_each_word": response_data["data"]["numberOfGuessAllowedForEachWord"],
        }
        return game_info

    def get_word(self, session_id):
        response = requests.post(
            self.game_end_point_url,
            data=json.dumps({
                "sessionId": session_id,
                "action": "nextWord",
            }),
            headers={"Content-Type": "application/json"}
        )
        if response.status_code != 200:
            raise APIError("status_code: {}".format(response.status_code))
        response_data = json.loads(response.text)
        word_info = {
            "word": response_data["data"]["word"],
            "total_word_count": response_data["data"]["totalWordCount"],
            "wrong_guess_count_of_current_word": response_data["data"]["wrongGuessCountOfCurrentWord"],
        }
        return word_info

    def make_guess(self, session_id, guess_letter):
        response = requests.post(
            self.game_end_point_url,
            data=json.dumps({
                "sessionId": session_id,
                "guess": guess_letter,
                "action": "guessWord",
            }),
            headers={"Content-Type": "application/json"}
        )
        if response.status_code != 200:
            raise APIError("status_code: {}".format(response.status_code))
        response_data = json.loads(response.text)
        word_info = {
            "word": response_data["data"]["word"],
            "total_word_count": response_data["data"]["totalWordCount"],
            "wrong_guess_count_of_current_word": response_data["data"]["wrongGuessCountOfCurrentWord"],
        }
        return word_info

    def get_result(self, session_id):
        response = requests.post(
            self.game_end_point_url,
            data=json.dumps({
                "sessionId": session_id,
                "action": "getResult",
            }),
            headers={"Content-Type": "application/json"}
        )
        if response.status_code != 200:
            raise APIError("status_code: {}".format(response.status_code))
        response_data = json.loads(response.text)
        result = {
            "total_word_count": response_data["data"]["totalWordCount"],
            "correct_word_count": response_data["data"]["correctWordCount"],
            "total_wrong_guess_count": response_data["data"]["totalWrongGuessCount"],
            "score": response_data["data"]["score"],
        }
        return result

    def submit_result(self, session_id):
        response = requests.post(
            self.game_end_point_url,
            data=json.dumps({
                "sessionId": session_id,
                "action": "submitResult",
            }),
            headers={"Content-Type": "application/json"}
        )
        if response.status_code != 200:
            raise APIError("status_code: {}".format(response.status_code))

        response_data = json.loads(response.text)
        result = {
            "datetime": response_data["data"]["datetime"],
            "total_word_count": response_data["data"]["totalWordCount"],
            "correct_word_count": response_data["data"]["correctWordCount"],
            "total_wrong_guess_count": response_data["data"]["totalWrongGuessCount"],
            "score": response_data["data"]["score"],
        }
        return result
