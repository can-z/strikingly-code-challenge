from challenge.exceptions import GameStartError


class Playthrough(object):

    def __init__(self, api_client, solver):
        self.api_client = api_client
        self.session_id = None
        self.wrong_guess_limit = None
        self.total_words = None
        self.solver = solver

    def start(self):
        if not self.session_id:
            game_info = self.api_client.start_game()
            self.session_id = game_info["session_id"]
            self.total_words = game_info["number_of_words_to_guess"]
            self.wrong_guess_limit = game_info["number_of_guess_allowed_for_each_word"]

        if not self.session_id:
            raise GameStartError()

        for _ in range(self.total_words):
            self.solver.solve_word(
                word=self.api_client.get_word(self.session_id)["word"],
                session_id=self.session_id,
                wrong_guess_limit=self.wrong_guess_limit)

        return self.api_client.get_result(self.session_id)
