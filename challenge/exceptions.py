class APIError(Exception):
    pass


class GameStartError(Exception):
    pass
