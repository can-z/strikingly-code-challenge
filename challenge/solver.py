# https://en.wikipedia.org/wiki/Letter_frequency
DEFAULT_GUESS_SEQUENCE = [
    "E", "T", "A", "O", "I",
    "N", "S", "H", "R", "D",
    "L", "C", "U", "M", "W",
    "F", "G", "Y", "P", "B",
    "V", "K", "J", "X", "Q",
    "Z"
]


class Solver(object):
    def __init__(self, api_client, guess_sequence=DEFAULT_GUESS_SEQUENCE):
        self.api_client = api_client
        self.guess_sequence = guess_sequence

    def solve_word(self, word, session_id, wrong_guess_limit):
        current_word_wrong_guess_count = 0
        index_of_letter_in_guess_sequence = 0
        while (current_word_wrong_guess_count < wrong_guess_limit and
                index_of_letter_in_guess_sequence < len(self.guess_sequence) and
                "*" in word):
            guess_result = self.api_client.make_guess(
                session_id,
                self.guess_sequence[index_of_letter_in_guess_sequence]
            )
            word = guess_result["word"]
            current_word_wrong_guess_count = guess_result["wrong_guess_count_of_current_word"]
            index_of_letter_in_guess_sequence += 1
